Homework 12 Balancing

run docker-compose up -d


UK
docker exec -it l12-uk-1 curl 172.18.236.100
172.18.236.100 (IP of the load balancer)

result will be NODE 1

US
docker exec -it l12-us-1 curl 172.18.236.100
172.18.236.100 (IP of the load balancer)

result will be NODE 2 & NODE 3

Same for all
docker exec -it l12-all-1 curl 172.18.236.100
result will be NODE 4